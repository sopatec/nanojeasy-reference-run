include <stepper.scad>
include <shaft_flag.scad>

cut_extra = 0.1;
$fn = 20;

FLAG = 1;
STOP_CW = 2;
STOP_CCW = 3;

print = 1;
print_select = STOP_CCW;

hard_stop_height = 3;
hard_stop_padding = 1.5;
hard_stop_stepper_tolerance = 1;
hard_stop_width = (stepper_hole_radius + hard_stop_padding) * 2;

hard_stop_gap_offset = stepper_front_offset + stepper_shaft_length - shaft_flag_height - 1;
hard_stop_gap_height = 4;

hard_stop_tower_width = 10;
hard_stop_tower_height = 14;
hard_stop_hit_tolerance = 0.15;

module _hard_stop() {
    difference() {
        union() {
            /* base plate */
            cube([stepper_width, hard_stop_width, hard_stop_height]);

            /* actual stopper */
            difference() {
                hull() {
                    translate([stepper_hole_radius + 2 * hard_stop_padding, 0, 0])
                        cube([stepper_width - 2 * (stepper_hole_radius 
                            + 2 * hard_stop_padding), hard_stop_width, hard_stop_height]);
                    translate([(stepper_width - hard_stop_tower_width) / 2, 0, 0])
                        cube([hard_stop_tower_width, hard_stop_width,
                            hard_stop_tower_height]);
                }
                translate([stepper_width / 2 - shaft_flag_end_width / 2
                        -hard_stop_hit_tolerance, -cut_extra,
                        hard_stop_gap_offset])
                    cube([stepper_width / 2, hard_stop_width + cut_extra,
                        hard_stop_gap_height + cut_extra]);
            }
        }

        /* remove overlap with stepper front */
        translate([stepper_width / 2, stepper_depth / 2, 0])
            cylinder(h = hard_stop_height + hard_stop_tower_height + cut_extra,
                r = stepper_front_radius + hard_stop_stepper_tolerance);

        /* holes */
        for (i = [0:1])
            translate([i ? (stepper_width - stepper_hole_offset) : stepper_hole_offset,
                    stepper_hole_offset, 0])
                cylinder(h = hard_stop_height + cut_extra, r = stepper_hole_radius);
    }
}

module hard_stop(side) {
    mirror([0, side, 0])
    _hard_stop();
}

if (!print) {
    translate([-stepper_width / 2, - stepper_depth / 2, 0])
        stepper();
}

if (!print || print_select == FLAG) {
    translate([0, 0, stepper_mid_length + stepper_front_offset
            + stepper_shaft_length - shaft_flag_height])
        shaft_flag(stepper_width / 2);
}

if (!print || print_select == STOP_CW) {
    translate([-stepper_width / 2, -stepper_depth / 2, stepper_mid_length])
        hard_stop(0);
}

if (!print || print_select == STOP_CCW) {
    translate([-stepper_width / 2, stepper_depth / 2, stepper_mid_length])
        hard_stop(1);
}

