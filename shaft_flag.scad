shaft_flag_thickness = 3;
shaft_flag_height = 8;
shaft_flag_radius = stepper_shaft_radius + shaft_flag_thickness;
shaft_flag_width = shaft_flag_radius * 2;

shaft_flag_end_width = 4;
shaft_flag_end_height = 2;
shaft_flag_end_length = 4;

shaft_flag_insert_offset = 1;

shaft_flag_shaft_tolerance = 0.1;

thread_insert_height = 3.3;
thread_insert_radius = 2.5;
thread_insert_hole_radius = 1.5;
thread_insert_hole_tolerance = 0.1;

module shaft_flag(shaft_flag_length) {
    difference() {
        hull() {
            /* plane for end of hull and start of end flag */
            translate([shaft_flag_length - shaft_flag_end_length, -shaft_flag_end_width / 2, 0])
                cube([0.01, shaft_flag_end_width, shaft_flag_end_height]);

            /* main sheath for shaft */
            cylinder(h = shaft_flag_height,
                r = stepper_shaft_radius + shaft_flag_thickness);

            /* extra for threaded insert */
            translate([-shaft_flag_radius - shaft_flag_insert_offset,
                    0, shaft_flag_height / 2]) {
                rotate([0, 90, 0]) {
                    cylinder(h = thread_insert_height, r = thread_insert_radius + 1);
                }
            }
        }
        translate([-shaft_flag_radius - shaft_flag_insert_offset - cut_extra, 0,
                shaft_flag_height / 2]) {
            rotate([0, 90, 0]) {
                /* hole for threaded insert */
                cylinder(h = thread_insert_height + cut_extra,
                    r = thread_insert_radius);

                /* hole for screw of threaded insert */
                cylinder(h = shaft_flag_radius + cut_extra, r = 1);
            }
        }
        /* hole for stepper shaft */
        translate([0, 0, -cut_extra])
            cylinder(h = shaft_flag_height + 2 * cut_extra,
                r = stepper_shaft_radius + shaft_flag_shaft_tolerance);
    }
    /* tip of flag */
    translate([shaft_flag_length - shaft_flag_end_length,
            -shaft_flag_end_width / 2, 0])
        cube([shaft_flag_end_length, shaft_flag_end_width, 
            shaft_flag_end_height]);
}
