cut_extra = 0.1;
stepper_fn = 12;

stepper_width = 28;
stepper_depth = 28;
stepper_mid_length = 50.5;

stepper_front_offset = 2;
stepper_front_radius = 11;

stepper_hole_offset = 2.5;
stepper_hole_radius = 1.5;

stepper_shaft_radius = 2.5;
stepper_shaft_length = 18; /* protrusion from front */

stepper_adapter_radius = 13;
stepper_adapter_offset = 5.3;

stepper_encoder_radius = 10;
stepper_encoder_height = 26;
stepper_encoder_offset = 10;

stepper_connector_offset = 1;
stepper_connector_width = 16;
stepper_connector_height = 6;
stepper_connector_depth = 3;

stepper_total_length = stepper_shaft_length + stepper_front_offset + stepper_mid_length + stepper_adapter_offset + stepper_encoder_offset;

module stepper() {
    /* encoder */
    translate([stepper_width / 2, stepper_depth / 2,
            - stepper_adapter_offset - stepper_encoder_offset]) {
        cylinder(r = stepper_encoder_radius, h = stepper_encoder_offset);
        translate([-stepper_encoder_radius, -(stepper_encoder_height - stepper_encoder_radius), 0])
            cube([stepper_encoder_radius * 2, stepper_encoder_height - stepper_encoder_radius,
                stepper_encoder_offset]);
    }

    /* encoder to stepper adapter */
    translate([stepper_width / 2, stepper_depth / 2, -stepper_adapter_offset])
        cylinder(h = stepper_adapter_offset, r = stepper_adapter_radius, $fn = stepper_fn * 2);

    /* connector */
    translate([(stepper_width - stepper_connector_width)/2, -stepper_connector_depth, stepper_connector_offset])
        cube([stepper_connector_width, stepper_connector_depth, stepper_connector_height]);

    difference() {
        /* main body */
        cube([stepper_width, stepper_depth, stepper_mid_length]);

        /* holes */
        for (x = [0:1])
            for (y = [0:1])
                translate([x ? (stepper_width - stepper_hole_offset) : stepper_hole_offset,
                    y ? (stepper_depth - stepper_hole_offset) : stepper_hole_offset,
                    -cut_extra])
                cylinder(h = stepper_mid_length + 2 * cut_extra, r = stepper_hole_radius, $fn = stepper_fn);
    }

    /* front and shaft */
    translate([stepper_width / 2, stepper_depth / 2, stepper_mid_length]) {
        cylinder(h = stepper_front_offset, r = stepper_front_radius, $fn = stepper_fn * 3);
        translate([0, 0, stepper_front_offset])
            cylinder(h = stepper_shaft_length, r = stepper_shaft_radius, $fn = stepper_fn);
    }
}
